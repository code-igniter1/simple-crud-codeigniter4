# Simple CRUD CodeIgniter 4
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/code-igniter1/simple-crud-codeigniter4.git`
2. Go inside the folder: `cd simple-crud-codeigniter4`
3. Run `cp env .env` and then set your database credentials
4. Run `php spark serve`
5. Open your favorite browser: http://localhost:8080/product

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add New Product

![Add New Product](img/add.png "Add New Product")

Edit Product Page

![Edit Product Page](img/edit.png "Edit Product Page")